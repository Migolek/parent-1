const configuration = require('../shared/store/configuration')
console.log(
  '🚀 ~ file: configuration.js ~ line 2 ~ configuration',
  configuration
)

export const state = {
  ...configuration.state,
}

export const getters = {
  ...configuration.getters,
}

export const mutations = {
  ...configuration.mutations,
}

export const actions = {
  ...configuration.actions,
}
